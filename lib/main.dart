import 'package:flutter/material.dart';
import 'package:mobile_core_app/screens/connecting.dart';
import 'package:mobile_core_app/screens/login.dart';
import 'package:mobile_core_app/routes.dart';
import 'package:dart_meteor/dart_meteor.dart';
import 'package:mobile_core_app/utils/store_token.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

// Create storage
final storage = new FlutterSecureStorage();
//Connect to meteor
MeteorClient meteor;
void main() {
  Routes.createRoute();
  runApp(App());
}

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  String _token;
  DateTime _expires;

  @override
  void initState() {
    super.initState();
    // Set dynamic ip address
    storage.read(key: 'ipAddress').then((value) {
      setState(() {
        meteor = MeteorClient.connect(url: 'http://$value');
      });
    });

    // Try to connect with previously saved token.
    storage.read(key: 'loginTokenExpires').then((tokenExpiresStr) {
      if (tokenExpiresStr != null) {
        int millisecond = int.parse(tokenExpiresStr);
        DateTime expires = DateTime.fromMillisecondsSinceEpoch(millisecond);
        setState(() {
          _expires = expires;
        });
        storage.read(key: 'loginToken').then((token) {
          if (token != null) {
            setState(() {
              _token = token;
            });
          }
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        WidgetsBinding.instance.focusManager.primaryFocus?.unfocus();
      },
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: StreamBuilder<DdpConnectionStatus>(
          stream: meteor.status(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              //checking connecting
              if (snapshot.data.connected) {
                if (_token != null && _expires != null) {
                  meteor
                      .loginWithToken(token: _token, tokenExpires: _expires)
                      .then((result) async {
                    await Token().storeMongoClientLoginResult(result);
                    Routes.sailor
                        .navigate('/home', params: {'username': "you sour"});
                  }).catchError((_) {});
                } else {
                  return Login();
                }
              }
              return Connecting();
            }
            return Connecting();
          },
        ),
        onGenerateRoute: Routes.sailor.generator(),
        navigatorKey: Routes.sailor.navigatorKey,
      ),
    );
  }
}
