import 'package:flutter/material.dart';
import 'package:mobile_core_app/utils/custom_login_input.dart';

class LoginInputWidget extends StatefulWidget {
  final Function(String) usernameCallBack;
  final Function(String) passwordCallBack;
  LoginInputWidget({this.usernameCallBack, this.passwordCallBack});
  @override
  _LoginInputWidgetState createState() => _LoginInputWidgetState();
}

class _LoginInputWidgetState extends State<LoginInputWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          decoration: kBoxDecorationStyle,
          alignment: Alignment.centerLeft,
          height: 60,
          child: TextFormField(
              validator: (value) {
                if (value.isEmpty) {
                  return 'Username is required';
                }
                return null;
              },
              onSaved: (text) {
                widget.usernameCallBack(text);
              },
              obscureText: false,
              style: TextStyle(color: Colors.white),
              cursorColor: Colors.white,
              decoration: InputDecoration(
                  errorStyle: TextStyle(
                    height: 0,
                    fontSize: 0,
                  ),
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.only(top: 14),
                  prefixIcon: Icon(Icons.mood, color: Colors.white),
                  hintText: 'Username',
                  // helperText: Text(),
                  hintStyle: kHintTextStyle)),
        ),
        SizedBox(
          height: 10,
        ),
        Container(
          decoration: kBoxDecorationStyle,
          alignment: Alignment.centerLeft,
          height: 60,
          child: TextFormField(
              validator: (value) {
                if (value.isEmpty) {
                  return 'Password is required';
                }
                return null;
              },
              onSaved: (text) {
                widget.passwordCallBack(text);
              },
              obscureText: true,
              style: TextStyle(color: Colors.white),
              cursorColor: Colors.white,
              decoration: InputDecoration(
                  errorStyle: TextStyle(
                    height: 0,
                    fontSize: 0,
                  ),
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.only(top: 14),
                  prefixIcon: Icon(Icons.lock, color: Colors.white),
                  hintText: 'Password',
                  hintStyle: kHintTextStyle)),
        ),
      ],
    );
  }
}
