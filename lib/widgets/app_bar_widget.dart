import 'package:flutter/material.dart';
import 'package:mobile_core_app/utils/constants.dart';

Widget appBarWidget({
  @required final String title,
  final IconData icon,
  final Color leadingColor,
  // final List<Color> backgroundColor,
  final Color backgroundColor,
}) {
  return AppBar(
    elevation: 0.0,
    leading: Builder(
      builder: (context) => IconButton(
        icon: Icon(Icons.menu),
        color: leadingColor,
        onPressed: () => Scaffold.of(context).openDrawer(),
      ),
    ),
    actions: <Widget>[
      Padding(
        padding: EdgeInsets.only(right: 5.0),
        child: IconButton(icon: Icon(Icons.notifications), onPressed: () {}),
      ),
    ],
    backgroundColor: backgroundColor,
    // flexibleSpace: Container(
    //   decoration: BoxDecoration(
    //     gradient: LinearGradient(
    //       begin: Alignment.centerLeft,
    //       end: Alignment.centerRight,
    //       colors: backgroundColor,
    //     ),
    //   ),
    // ),
    title: Text(
      title,
      style: TextStyle(fontFamily: CommonFonts.header),
    ),
    centerTitle: true,
  );
}

Widget commonAppBarWidget({
  @required final String title,
  // @required final List<Color> backgroundColor,
  final Color backgroundColor,
}) {
  return AppBar(
    elevation: 0.0,
    backgroundColor: backgroundColor,
    // flexibleSpace: Container(
    //   decoration: BoxDecoration(
    //     gradient: LinearGradient(
    //       begin: Alignment.centerLeft,
    //       end: Alignment.centerRight,
    //       colors: backgroundColor,
    //     ),
    //   ),
    // ),
    title: Text(title, style: TextStyle(fontFamily: CommonFonts.header)),
    centerTitle: true,
  );
}
