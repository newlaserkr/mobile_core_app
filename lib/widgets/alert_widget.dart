// import 'package:flutter/material.dart';

// class AlertHelper {
//   static show(
//           {context,
//           String title,
//           String description,
//           Color headColor = Colors.white,
//           Color bodyColor = Colors.blue,
//           String alertImageHeader = 'assets/images/logo.png'}) =>
//       showDialog(
//           context: context,
//           builder: (context) => BasicAlert(
//               title: title,
//               description: description,
//               headColor: headColor,
//               bodyColor: bodyColor,
//               alertImageHeader: alertImageHeader));
// }

// class BasicAlert extends StatelessWidget {
//   final String title;
//   final String description;
//   Color headColor;
//   Color bodyColor;
//   String alertImageHeader;
//   BasicAlert(
//       {@required this.title,
//       @required this.description,
//       this.headColor,
//       this.bodyColor,
//       this.alertImageHeader});
//   @override
//   Widget build(BuildContext context) {
//     return Dialog(
//       shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
//       elevation: 0,
//       backgroundColor: Colors.transparent,
//       child: _buildChild(context),
//     );
//   }

//   _buildChild(BuildContext context) => Container(
//         height: 250.0,
//         decoration: BoxDecoration(
//             color: bodyColor,
//             shape: BoxShape.rectangle,
//             borderRadius: BorderRadius.only(
//               topLeft: Radius.circular(12.0),
//               topRight: Radius.circular(12.0),
//             )),
//         child: Column(
//           children: <Widget>[
//             Container(
//               child: Padding(
//                 padding: const EdgeInsets.all(12.0),
//                 child: Image.asset(
//                   alertImageHeader,
//                   height: 120,
//                   width: 120,
//                 ),
//               ),
//               width: double.infinity,
//               decoration: BoxDecoration(
//                 color: headColor,
//                 shape: BoxShape.rectangle,
//                 borderRadius: BorderRadius.only(
//                   topLeft: Radius.circular(12),
//                   topRight: Radius.circular(12),
//                 ),
//               ),
//             ),
//             SizedBox(
//               height: 24,
//             ),
//             Text(
//               title,
//               style: TextStyle(
//                   fontSize: 20,
//                   color: Colors.white,
//                   fontWeight: FontWeight.bold),
//             ),
//             SizedBox(
//               height: 8,
//             ),
//             Padding(
//               padding: const EdgeInsets.only(right: 16, left: 16),
//               child: Text(
//                 description,
//                 style: TextStyle(color: Colors.white),
//                 textAlign: TextAlign.center,
//               ),
//             ),
//             // SizedBox(
//             //   height: 24,
//             // ),
//             // Row(
//             //   mainAxisSize: MainAxisSize.min,
//             //   children: <Widget>[
//             //     FlatButton(
//             //       onPressed: () {
//             //         Navigator.of(context).pop();
//             //       },
//             //       child: Text('No'),
//             //       textColor: Colors.white,
//             //     ),
//             //     SizedBox(
//             //       width: 8,
//             //     ),
//             //     RaisedButton(
//             //       onPressed: () {
//             //         return Navigator.of(context).pop(true);
//             //       },
//             //       child: Text('Yes'),
//             //       color: Colors.white,
//             //       textColor: Colors.redAccent,
//             //     )
//             //   ],
//             // )
//           ],
//         ),
//       );
// }
