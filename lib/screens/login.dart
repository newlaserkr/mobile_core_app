import 'package:flutter/material.dart';
import 'package:mobile_core_app/utils/store_token.dart';
import 'package:mobile_core_app/widgets/login_input_widget.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:mobile_core_app/routes.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:mobile_core_app/main.dart';
import 'package:dart_meteor/dart_meteor.dart';
import 'package:mobile_core_app/utils/alert_style.dart';
import 'package:mobile_core_app/utils/constants.dart';
import 'package:animate_do/animate_do.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  int copyRight = 0x00A9;
  bool isLoading = false;
  final _formkey = GlobalKey<FormState>();
  String _username = "";
  String _password = "";
  usernameCallBack(val) {
    setState(() {
      _username = val;
    });
  }

  passwordCallBack(val) {
    setState(() {
      _password = val;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      height: double.infinity,
      width: double.infinity,
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: CommonColors.kitGradients)),
      child: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              ElasticIn(
                duration: Duration(milliseconds: 1200),
                child: Container(
                  margin: EdgeInsets.only(top: 60.0),
                  child: Image(
                    width: 150.0,
                    height: 150.0,
                    image: AssetImage('assets/images/logo.png'),
                  ),
                ),
              ),
              ElasticInLeft(
                duration: Duration(milliseconds: 1200),
                child: Container(
                  margin: EdgeInsets.symmetric(vertical: 20.0),
                  alignment: Alignment.center,
                  child: Text('RABBIT',
                      style: TextStyle(
                          color: CommonColors.core,
                          letterSpacing: 5.0,
                          fontSize: 50.0,
                          fontFamily: CommonFonts.header)),
                ),
              ),
              if (isLoading)
                SpinKitThreeBounce(
                  color: CommonColors.core,
                  size: 20.0,
                ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 60.0),
                child: Divider(
                  color: CommonColors.secondary,
                ),
              ),
              Container(
                margin: EdgeInsets.all(20.0),
                child: Form(
                  key: _formkey,
                  child: Column(
                    children: <Widget>[
                      LoginInputWidget(
                        usernameCallBack: usernameCallBack,
                        passwordCallBack: passwordCallBack,
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(vertical: 25.0),
                        width: double.infinity,
                        child: RaisedButton(
                          elevation: 3.0,
                          onPressed: () async {
                            if (_formkey.currentState.validate()) {
                              _formkey.currentState.save();
                              setState(() {
                                isLoading = true;
                              });
                              await meteor
                                  .loginWithPassword(_username, _password,
                                      delayOnLoginErrorSecond: 2)
                                  .then((result) async {
                                setState(() {
                                  isLoading = false;
                                });
                                Routes.sailor.navigate('/home',
                                    params: {'username': "you sour"});
                                Token().storeMongoClientLoginResult(result);
                              }).catchError((err) {
                                if (err is MeteorError) {
                                  setState(() {
                                    isLoading = false;
                                  });
                                  Alert(
                                      context: context,
                                      style: alertErrorStyle,
                                      title: "ERROR",
                                      desc: err.message,
                                      buttons: []).show();
                                }
                              });
                            } else {
                              Alert(
                                  context: context,
                                  style: alertWarningStyle,
                                  title: "WARNING",
                                  desc:
                                      'Please, Check Your Username and Password Again !',
                                  buttons: []).show();
                            }
                          },
                          padding: EdgeInsets.all(15.0),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                          color: CommonColors.core,
                          child: Text(
                            'LOGIN',
                            style: TextStyle(
                              color: CommonColors.primary,
                              letterSpacing: 1.5,
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold,
                              fontFamily: CommonFonts.header,
                            ),
                          ),
                        ),
                      ),
                      Text(
                        'Copyright \u00a9 2020 Rabbit Technology . All Right Reserved.',
                        style: TextStyle(
                            color: CommonColors.core,
                            fontFamily: CommonFonts.body,
                            fontSize: 14.0),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    ));
  }
}
