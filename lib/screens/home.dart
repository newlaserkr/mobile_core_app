import 'package:flutter/material.dart';
import 'package:mobile_core_app/screens/slide_drawer.dart';
import 'package:mobile_core_app/screens/connecting.dart';
import 'package:dart_meteor/dart_meteor.dart';
import 'package:mobile_core_app/main.dart';
import 'package:mobile_core_app/utils/constants.dart';
import 'package:mobile_core_app/widgets/app_bar_widget.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<DdpConnectionStatus>(
      stream: meteor.status(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          //checking connecting
          if (snapshot.data.connected) {
            return Content();
          }
          return Connecting();
        }
        return Connecting();
      },
    );
  }
}

class Content extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:
          appBarWidget(title: "Home", backgroundColor: CommonColors.primary),
      drawer: SlideDrawer(),
      body: WillPopScope(
        onWillPop: () async => false,
        child: SafeArea(
          child: Column(
            children: <Widget>[
              Center(child: Text('Home')),
            ],
          ),
        ),
      ),
    );
  }
}
