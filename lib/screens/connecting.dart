import 'package:flutter/material.dart';
import 'package:mobile_core_app/main.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:mobile_core_app/utils/alert_style.dart';
import 'package:mobile_core_app/utils/constants.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:mobile_core_app/utils/store_token.dart';
import 'package:animate_do/animate_do.dart';
import 'dart:io';

import '../main.dart';

class Connecting extends StatelessWidget {
  String _ipAddress;
  final _formkey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: CommonColors.kitGradients)),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              ElasticIn(
                duration: Duration(milliseconds: 1200),
                child: Container(
                  margin: EdgeInsets.only(top: 60.0),
                  child: Image(
                    width: 150.0,
                    height: 150.0,
                    image: AssetImage('assets/images/logo.png'),
                  ),
                ),
              ),
              SizedBox(
                height: 12.0,
              ),
              SpinKitThreeBounce(
                color: CommonColors.core,
                size: 20.0,
              ),
              SizedBox(
                height: 12.0,
              ),
              Text(
                'Connecting to server...',
                style: TextStyle(
                  color: CommonColors.core,
                  fontSize: 16.0,
                  fontFamily: CommonFonts.body,
                ),
              ),
              SizedBox(
                height: 12.0,
              ),
              Container(
                margin: EdgeInsets.all(10.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: RaisedButton(
                        onPressed: () {
                          meteor.reconnect();
                        },
                        padding: EdgeInsets.all(15.0),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        color: CommonColors.core,
                        child: Text(
                          'Reconnet',
                          style: TextStyle(
                            color: CommonColors.primary,
                            letterSpacing: 1.5,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                            fontFamily: CommonFonts.body,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10.0,
                    ),
                    Expanded(
                      child: RaisedButton(
                        onPressed: () {
                          Alert(
                              context: context,
                              title: "Setting",
                              style: AlertStyle(
                                  titleStyle: TextStyle(
                                      color: CommonColors.primary,
                                      fontSize: 28.0,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: CommonFonts.header)),
                              content: Column(
                                children: <Widget>[
                                  Form(
                                    key: _formkey,
                                    child: TextFormField(
                                        cursorColor: CommonColors.primary,
                                        onSaved: (text) {
                                          _ipAddress = text;
                                        },
                                        decoration: InputDecoration(
                                          errorStyle: TextStyle(
                                            height: 0,
                                            fontSize: 0,
                                          ),
                                          focusedBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                width: 3.0,
                                                color: CommonColors.primary),
                                          ),
                                          contentPadding:
                                              EdgeInsets.only(top: 14),
                                          prefixIcon: Icon(
                                              Icons.settings_ethernet,
                                              color: CommonColors.primary),
                                          hintText: 'IP Address',
                                        )),
                                  ),
                                ],
                              ),
                              buttons: [
                                DialogButton(
                                  color: CommonColors.primary,
                                  onPressed: () async {
                                    _formkey.currentState.save();

                                    await Token()
                                        .storeIpAddressResult(_ipAddress);

                                    Navigator.pop(context);
                                    await Alert(
                                        context: context,
                                        style: alertSuccessStyle,
                                        title: "SUCCESS",
                                        desc:
                                            'Set up IP Address is Completed , You need to close app and open again !',
                                        buttons: [
                                          // DialogButton(
                                          //   color: CommonColors.danger,
                                          //   radius: BorderRadius.circular(30.0),
                                          //   child: Text(
                                          //     "Cancel",
                                          //     style: TextStyle(
                                          //         color: CommonColors.core,
                                          //         fontSize: 16.0),
                                          //   ),
                                          //   onPressed: () =>
                                          //       Navigator.pop(context),
                                          //   // width: 120,
                                          // ),
                                          DialogButton(
                                            color: CommonColors.success,
                                            radius: BorderRadius.circular(30.0),
                                            child: Text(
                                              "Close App",
                                              style: TextStyle(
                                                  color: CommonColors.core,
                                                  fontSize: 16.0),
                                            ),
                                            onPressed: () => exit(0),
                                          )
                                        ]).show();
                                  },
                                  radius: BorderRadius.circular(30.0),
                                  child: Text(
                                    "Set Up",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16.0,
                                        fontFamily: CommonFonts.body),
                                  ),
                                )
                              ]).show();
                        },
                        padding: EdgeInsets.all(15.0),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        color: CommonColors.core,
                        child: Text(
                          'Setting',
                          style: TextStyle(
                            color: CommonColors.primary,
                            letterSpacing: 1.5,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                            fontFamily: CommonFonts.body,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
