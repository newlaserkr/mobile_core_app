import 'package:flutter/material.dart';
import 'package:mobile_core_app/screens/connecting.dart';
import 'package:dart_meteor/dart_meteor.dart';
import 'package:mobile_core_app/main.dart';
import 'package:mobile_core_app/widgets/app_bar_widget.dart';
import 'package:mobile_core_app/utils/constants.dart';

class Profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<DdpConnectionStatus>(
      stream: meteor.status(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          //checking connecting
          if (snapshot.data.connected) {
            return Content();
          }
          return Connecting();
        }
        return Connecting();
      },
    );
  }
}

class Content extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: commonAppBarWidget(
          title: 'Profile', backgroundColor: CommonColors.primary),
      // AppBar(
      //   backgroundColor: CommonColors.primary,
      //   title: Text('Profile'),
      //   centerTitle: true,
      // ),
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Center(child: Text('Profile Page')),
          ],
        ),
      ),
    );
  }
}
