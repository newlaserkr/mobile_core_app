import 'package:flutter/material.dart';
import 'package:mobile_core_app/routes.dart';
import 'package:mobile_core_app/main.dart';
import 'package:mobile_core_app/utils/constants.dart';
import 'package:mobile_core_app/utils/store_token.dart';

class SlideDrawer extends StatefulWidget {
  @override
  _SlideDrawerState createState() => _SlideDrawerState();
}

class _SlideDrawerState extends State<SlideDrawer> {
  @override
  Widget build(BuildContext context) {
    String _username;
    String _email;
    if (meteor.userCurrentValue() != null) {
      _username = meteor.userCurrentValue()['username'];
      _email = meteor.userCurrentValue()['emails'] != null
          ? meteor.userCurrentValue()['emails'][0]['address']
          : 'youremail@gmail.com';
    }
    return Container(
      child: SafeArea(
        child: Drawer(
          child: ListView(
            children: <Widget>[
              Container(
                color: CommonColors.primary,
                child: Center(
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        width: 80.0,
                        height: 80.0,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: CommonColors.secondary,
                            width: 2.0,
                          ),
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: AssetImage('assets/images/logo.png')),
                          borderRadius: BorderRadius.all(Radius.circular(45.0)),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        _username.toUpperCase(),
                        style: TextStyle(
                            color: CommonColors.core,
                            fontWeight: FontWeight.bold,
                            fontSize: 20.0,
                            fontFamily: CommonFonts.header),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(_email,
                          style: TextStyle(
                              color: CommonColors.core,
                              fontSize: 16.0,
                              fontFamily: CommonFonts.body)),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
              ),
              ListTile(
                leading: Icon(Icons.face),
                title: Text(
                  'Profile',
                  style: TextStyle(fontFamily: CommonFonts.body),
                ),
                onTap: () {
                  Navigator.pop(context);
                  Routes.sailor("/profile");
                },
              ),
              ListTile(
                leading: Icon(Icons.arrow_back),
                title: Text(
                  'Logout',
                  style: TextStyle(fontFamily: CommonFonts.body),
                ),
                onTap: () async {
                  await Token().deleteMongoClientLoginResult();
                  meteor.logout();
                  Routes.sailor("/login");
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
