import 'package:mobile_core_app/screens/home.dart';
import 'package:mobile_core_app/screens/profile.dart';
import 'package:mobile_core_app/screens/login.dart';
import 'package:sailor/sailor.dart';

class Routes {
  static final sailor = Sailor();
  static void createRoute() {
    sailor.addRoutes([
      SailorRoute(
        name: '/login',
        builder: (context, args, params) {
          return Login();
        },
      ),
      SailorRoute(
          name: '/home',
          builder: (context, args, params) {
            return Home();
          },
          params: [
            SailorParam(
                name: 'username', isRequired: true, defaultValue: 'sour')
          ]),
      SailorRoute(
        name: '/profile',
        builder: (context, args, params) {
          return Profile();
        },
      ),
    ]);
  }
}
