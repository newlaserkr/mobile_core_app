import 'package:flutter/material.dart';
import 'package:mobile_core_app/utils/constants.dart';

final kHintTextStyle = TextStyle(
  color: Colors.white54,
  fontFamily: CommonFonts.body,
);

final kLabelStyle = TextStyle(
  color: CommonColors.core,
  fontWeight: FontWeight.bold,
  fontFamily: CommonFonts.body,
);

final kBoxDecorationStyle = BoxDecoration(
  color: CommonColors.primary,
  borderRadius: BorderRadius.circular(5.0),
  boxShadow: [
    BoxShadow(
      color: Colors.black12,
      blurRadius: 5.0,
      offset: Offset(0, 2),
    ),
  ],
);
