import 'package:flutter/material.dart';
import 'dart:math';
import 'dart:ui';

class CommonColors {
  static const core = Color(0xFFFFFFFF);
  static const primary = Color(0xFFf47526);
  static const secondary = Color(0xFFffab00);
  static const accent = Color(0xFF000000);
  static const black = Color(0xFF000000);
  static const success = Color(0xFF00E676);
  static const warning = Color(0xFFF57C00);
  static const danger = Color(0xFFFF1744);
  static const info = Color(0xFF448AFF);

  static List<Color> kitGradients = [primary, secondary];
  static List<Color> kitGradients2 = [
    Color(0xffb7ac50),
    Colors.orange.shade900
  ];

  //randomcolor
  static final Random _random = new Random();

  /// Returns a random color.
  static Color next() {
    return new Color(0xFF000000 + _random.nextInt(0x00FFFFFF));
  }
}

class CommonFonts {
  static const header = 'SF Pro Display';
  static const body = 'Bahnschrift';
}
