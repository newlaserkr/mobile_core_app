import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class Token {
  // Create storage
  final storage = new FlutterSecureStorage();
  storeMongoClientLoginResult(result) async {
    if (result != null) {
      await storage.write(key: 'loginToken', value: result.token);
      await storage.write(
        key: 'loginTokenExpires',
        value: result.tokenExpires.millisecondsSinceEpoch.toString(),
      );
    }
  }

  storeIpAddressResult(String result) async {
    if (result != null) {
      await storage.write(key: 'ipAddress', value: result);
    }
  }

  Future<String> readIpAddressResult() async {
    String url;
    storage.read(key: 'ipAddress').then((value) {
      url = value;
    });
    return url;
  }

  deleteMongoClientLoginResult() async {
    await storage.delete(key: 'loginToken');
    await storage.delete(key: 'loginTokenExpires');
  }
}
