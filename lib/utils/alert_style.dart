import 'package:mobile_core_app/utils/constants.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:flutter/material.dart';

final alertInfoStyle = AlertStyle(
  animationType: AnimationType.fromBottom,
  isCloseButton: false,
  isOverlayTapDismiss: true,
  descStyle: TextStyle(
    fontSize: 16.0,
    color: Colors.grey,
    fontFamily: CommonFonts.body,
  ),
  animationDuration: Duration(milliseconds: 400),
  alertBorder: RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(15.0),
    // side: BorderSide(
    //   color: Colors.grey,
    // ),
  ),
  titleStyle: TextStyle(
      color: CommonColors.info,
      fontSize: 40.0,
      fontWeight: FontWeight.bold,
      fontFamily: CommonFonts.header),
);

final alertWarningStyle = AlertStyle(
  animationType: AnimationType.fromBottom,
  isCloseButton: false,
  isOverlayTapDismiss: true,
  descStyle: TextStyle(
    fontSize: 16.0,
    color: Colors.grey,
    fontFamily: CommonFonts.body,
  ),
  animationDuration: Duration(milliseconds: 400),
  alertBorder: RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(15.0),
    // side: BorderSide(
    //   color: Colors.grey,
    // ),
  ),
  titleStyle: TextStyle(
      color: CommonColors.warning,
      fontSize: 40.0,
      fontWeight: FontWeight.bold,
      fontFamily: CommonFonts.header),
);

final alertErrorStyle = AlertStyle(
  animationType: AnimationType.fromBottom,
  isCloseButton: false,
  isOverlayTapDismiss: true,
  descStyle: TextStyle(
    fontSize: 16.0,
    color: Colors.grey,
    fontFamily: CommonFonts.body,
  ),
  animationDuration: Duration(milliseconds: 400),
  alertBorder: RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(15.0),
    // side: BorderSide(
    //   color: Colors.grey,
    // ),
  ),
  titleStyle: TextStyle(
    color: CommonColors.danger,
    fontSize: 40.0,
    fontWeight: FontWeight.bold,
    fontFamily: CommonFonts.header,
  ),
);

final alertSuccessStyle = AlertStyle(
  animationType: AnimationType.fromBottom,
  isCloseButton: false,
  isOverlayTapDismiss: true,
  descStyle: TextStyle(
    fontSize: 16.0,
    color: Colors.grey,
    fontFamily: CommonFonts.body,
  ),
  animationDuration: Duration(milliseconds: 400),
  alertBorder: RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(15.0),
    // side: BorderSide(
    //   color: Colors.grey,
    // ),
  ),
  titleStyle: TextStyle(
    color: CommonColors.success,
    fontSize: 40.0,
    fontWeight: FontWeight.bold,
    fontFamily: CommonFonts.header,
  ),
);
